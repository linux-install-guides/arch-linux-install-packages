# My Arch-Linux i3 setup

<hr>

### Packages config available:

* i3-gaps
* i3-scrot
* picom
* polybar
* rofi
* alacritty
* gtk3.css (gnome, xfce terminal padding)
* dunst (shamelessly copied from endeavourOS config)
* ranger
* archlinux-wallpaper (used as wallpaper)
* pywal

### Screanshot 1: Custom Wallpaper and neofetch
<br>

![Wallpaper and neofetch](./pics/1.png)

<br><br>

### Screanshot 2: Archlinux-wallpaper and pfetch
<br>

![pfetch](./pics/2.png)

<br><br>

### Screenshot 3: Ranger, vim and file-roller(gnome file manager)
<br>

![ranger](pics/3.png)

<br><br>
